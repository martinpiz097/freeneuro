- 0.1 Alpha
	- Creacion de clases Neuron, Cap y ActivationFunction
	- Se realizan pruebas de funcionamiento de codigo en clase Neuro resultando existosas

- 0.2 Beta
    - Se agrega clase NeuroNetwork para manejar una red de clases Neuron
    - Clases Neuron y Cap implementan Interfaz Operable para ser manejadas
    de una manera mas generica.
