package org.freeneuro.exceptions;

public class NeuronException extends RuntimeException {
    public NeuronException() {
        super();
    }

    public NeuronException(String message) {
        super(message);
    }
}
