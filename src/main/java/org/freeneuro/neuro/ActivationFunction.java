package org.freeneuro.neuro;

public abstract class ActivationFunction {
    public boolean getBooleanResult(Neuron neuron, double result) {
        return getBinaryResult(neuron, result) > 0;
    }

    public abstract byte getBinaryResult(Neuron neuron, double result);
}
