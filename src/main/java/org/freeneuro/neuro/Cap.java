package org.freeneuro.neuro;

import org.freeneuro.exceptions.NeuronException;
import org.freeneuro.neuro.interfaces.Operable;

import java.util.ArrayList;

public class Cap implements Operable {
    private ArrayList<Neuron> listNeurons;

    public Cap() {
        this.listNeurons = new ArrayList<>();
    }

    public Cap(ArrayList<Neuron> listNeurons) {
        this.listNeurons = listNeurons;
    }

    public boolean hasNeurons() {
        return !listNeurons.isEmpty();
    }

    public void addNeuron(Neuron neuron) {
        listNeurons.add(neuron);
    }

    public int getNeuronCount() {
        return listNeurons.size();
    }

    public ArrayList<Neuron> getListNeurons() {
        return listNeurons;
    }

    @Override
    public boolean isActive(double result) {
        return result > 0;
    }

    @Override
    public double getOutputValue(double... entryValues) {
        if (!hasNeurons())
            throw new NeuronException("Cap haven't neurons");

        double output = 0;

        Neuron neuron;
        double neuronOutput = 0;
        for (int i = 0; i < listNeurons.size(); i++) {
            neuron = listNeurons.get(i);
            neuronOutput = neuron.getOutputValue(entryValues);
            if (neuron.isActive(neuronOutput))
                output+=neuronOutput;
        }
        return output;

    }

}
