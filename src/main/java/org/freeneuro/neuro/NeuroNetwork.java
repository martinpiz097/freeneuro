package org.freeneuro.neuro;

import org.freeneuro.neuro.interfaces.Operable;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class NeuroNetwork {
    private ArrayList<Operable> listOperables;

    public NeuroNetwork() {
        listOperables = new ArrayList<>();
    }

    public void addNode(Operable node) {
        listOperables.add(node);
    }

    public int getNodes() {
        return listOperables.size();
    }

    public long getCaps() {
        return listOperables
                .parallelStream()
                .filter(node -> node instanceof Cap).count();
    }

    public long getNeurons() {
        AtomicInteger neuronsCount = new AtomicInteger();
        listOperables.parallelStream()
                .forEach(node ->{
                    if (node instanceof Cap)
                        neuronsCount.addAndGet(((Cap) node).getNeuronCount());
                    else
                        neuronsCount.getAndIncrement();
                });
        return neuronsCount.intValue();
    }

    public NeuroNetwork(ArrayList<Operable> listOperables) {
        this.listOperables = listOperables;
    }

    public void clear() {
        listOperables.clear();
    }

}
