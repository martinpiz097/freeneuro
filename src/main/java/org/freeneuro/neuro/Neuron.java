package org.freeneuro.neuro;

import org.freeneuro.exceptions.NeuronException;
import org.freeneuro.neuro.interfaces.Operable;

public class Neuron implements Operable {
    private final double baias;
    private final ActivationFunction activationFunction;
    private boolean isOn;

    public Neuron(double baias, ActivationFunction activationFunction) {
        this.baias = baias;
        if (activationFunction == null)
            throw new NeuronException("Activation Function cant be null");
        this.activationFunction = activationFunction;
        isOn = true;
    }

    private boolean toBool(double value) {
        return value > 0;
    }

    public boolean isOn() {
        return isOn;
    }

    public void turnOn() {
        isOn = true;
    }

    public void shutdown() {
        isOn = false;
    }

    public double getBaias() {
        return baias;
    }

    public ActivationFunction getActivationFunction() {
        return activationFunction;
    }

    @Override
    public boolean isActive(double result) {
        return activationFunction.getBooleanResult(this, result);
    }

    @Override
    public double getOutputValue(double... entryValues) {
        if (!isOn)
            return 0;
        if (entryValues == null)
            throw new NeuronException("Entry values cant be null");

        double result = 0;

        double entry;
        for (int i = 0; i < entryValues.length; i++) {
            entry = entryValues[i];
            if (toBool(entry))
                result += entry;
        }
        result+=baias;
        return result;
    }

}
