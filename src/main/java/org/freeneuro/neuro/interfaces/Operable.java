package org.freeneuro.neuro.interfaces;

public interface Operable {
    public boolean isActive(double result);
    public double getOutputValue(double... entryValues);
}
