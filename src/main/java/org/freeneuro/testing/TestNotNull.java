package org.freeneuro.testing;

import org.freeneuro.neuro.ActivationFunction;
import org.freeneuro.neuro.Neuron;

public class TestNotNull {
    public static void main(String[] args) {
        ActivationFunction function = new ActivationFunction() {
            @Override
            public byte getBinaryResult(Neuron neuron, double result) {
                return (byte) (result > 0 ? 1 : 0);
            }
        };
        Neuron neuron = new Neuron(-6, function);
        double outValue = neuron.getOutputValue(4, 5);
        boolean active = neuron.isActive(outValue);
        System.out.println(outValue);
        System.out.println(active);

    }
}
